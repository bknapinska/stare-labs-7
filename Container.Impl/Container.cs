﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container.Impl
{
    public class Container : PK.Container.IContainer
    {
        private Dictionary<Type, object> objects = new Dictionary<Type, object>();
        private bool test = false;
        public Container()
        {
        }


        public void Register(System.Reflection.Assembly assembly)
        {
        }

        public void Register(Type type)
        {
            this.test = true;
        }

        public void Register<T>(T impl) where T : class
        {
        }

        public void Register<T>(Func<T> provider) where T : class
        {
        }

        public T Resolve<T>() where T : class
        {
            throw new PK.Container.UnresolvedDependenciesException();
        }

        public object Resolve(Type type)
        {
            if (this.test)
            {
                return (object) new Main.Implementation.CMain();
            }
            throw new PK.Container.UnresolvedDependenciesException();
        }
    }
}
