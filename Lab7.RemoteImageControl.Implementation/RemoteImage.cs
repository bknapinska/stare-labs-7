﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab7.RemoteImageControl.Contract;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using System.Net;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImage : IRemoteImage
    {
        private Control control = new RemoteImageCtrl();
        public Control Control
        {
            get { return control; }
        }

        public void Load(string url)
        {

            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(url));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            (control as RemoteImageCtrl).image1.Source = imageSource;
        }
    }
}

