﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MExpectedException = Microsoft.VisualStudio.TestTools.UnitTesting.ExpectedExceptionAttribute;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using ExpectedException = NUnit.Framework.ExpectedExceptionAttribute;
using Lab7.Infrastructure;
using PK.Container;
using System.Reflection;
using System.Collections.Generic;
using TinyMVVMHelper;
using Lab7.RemoteImageControl.Contract;
using PK.Test;

namespace Lab7.Test
{
    [TestFixture, RequiresSTA]
    [TestClass]
    public class P1_Configuration
    {
        [Test]
        [TestMethod]
        public void P1__ContainerFactory_Should_Return_IContainer_Implementation()
        {
            // Arrange
            var container = LabDescriptor.ContainerFactory();

            // Assert
            Assert.That(container, Is.Not.Null);
            Assert.That(container, Is.InstanceOf<IContainer>());
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Implementation_Of_AdressControl_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.AddressControlSpec;
            var component = LabDescriptor.AddressControlImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Implementation_Of_RemoteImageControl_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.RemoteImageControlSpec;
            var component = LabDescriptor.RemoteImageControlImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P1__AddressControl_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.AddressControlSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework"));
        }

        [Test]
        [TestMethod]
        public void P1__AddressControl_Implementation_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.AddressControlImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework")
                                        .Or.Property("Name").EqualTo(LabDescriptor.AddressControlSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(Assembly.GetAssembly(typeof(ViewModel)).GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P1__RemoteImageControl_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.RemoteImageControlSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework")
                                        .Or.Property("Name").EqualTo(LabDescriptor.AddressControlSpec.GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P1__RemoteImageControl_Implementation_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.RemoteImageControlImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework")
                                        .Or.Property("Name").EqualTo(LabDescriptor.RemoteImageControlSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(LabDescriptor.AddressControlSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(Assembly.GetAssembly(typeof(ViewModel)).GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P1__AddressControl_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums_Or_Dependent_Types()
        {
            // Arrange
            var assembly = LabDescriptor.AddressControlSpec;
            
            // Assert
            Helpers.Should_Have_Only_Interfaces_Or_Enums_Or_Dependent_Types(assembly);
        }

        [Test]
        [TestMethod]
        public void P1__RemoteImageControl_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums_Or_Dependent_Types()
        {
            // Arrange
            var assembly = LabDescriptor.RemoteImageControlSpec;

            // Assert
            Helpers.Should_Have_Only_Interfaces_Or_Enums_Or_Dependent_Types(assembly);
        }

        [Test]
        [TestMethod]
        public void P1__AddressControl_Implementation_Library_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.AddressControlImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.AddressControlSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                if (type.Name.Equals("GeneratedInternalTypeHelper"))
                    continue;
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        public void P1__RemoteImageControl_Implementation_Library_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.RemoteImageControlImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.RemoteImageControlSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                if (type.Name.Equals("GeneratedInternalTypeHelper"))
                    continue;
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        public void P1__RemoteImageControl_Should_Depend_On_MainComponent()
        {
            // Arrange
            var container = LabDescriptor.ContainerFactory();
            object result = null;

            // Act
            container.Register(LabDescriptor.RemoteImageControlImpl);
            try
            {
                result = container.Resolve<IRemoteImage>();
            }
            catch (UnresolvedDependenciesException ex) { }

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Be_Properly_Configured_In_ConfigureApp()
        {
            // Arrange
            var container = Configuration.ConfigureApp();
            
            // Act
            var result = container.Resolve<IRemoteImage>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Control, Is.Not.Null);
        }
    }
}
